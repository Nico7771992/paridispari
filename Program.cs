﻿/* Esercizio 2:

Creare un programma che chiede all'utente un numero e ti dice se è pari o dispari

*/

using System;

namespace Pari
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero;
            int scelta;

            Console.WriteLine("Benvenuto nel trombatore di numeri");
            do
            {
                Console.Write("Inserisci un numero per verificare se è pari o dispari : ");

                numero = Convert.ToInt32(Console.ReadLine());

                if (numero % 2 == 0)
                {
                    Console.WriteLine("Questo è un numero pari ");
                }
                else
                {
                    Console.WriteLine("Questo è un numero dispari ");
                }
                Console.WriteLine("Vuoi continuare? Se vuoi chiudere premi 1");
                scelta = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
            }
            while (scelta != 1);
        }
    }
}
